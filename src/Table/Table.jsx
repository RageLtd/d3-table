import React from 'react';
import { hot } from 'react-hot-loader';
import cn from 'classnames';

import DataGrid from './DataGrid';

import styles from './Table.css';

function Table({ data, columns, rowHeight }) {
  return (
    <div className={cn(styles.viewport)}>
      <DataGrid columns={columns} rowHeight={rowHeight} rows={data} />
    </div>
  );
}

export default hot(module)(Table);
