import React from 'react';
import Cell from '../Cell';

import cn from 'classnames';

import styles from './Column.css';

export default function Column({ column, rowHeight, rows }) {
  const { cellRenderer, header, headerRenderer, key } = column;
  return (
    <div className={cn(styles.column)}>
      {header && headerRenderer && <Cell data={headerRenderer(header)} />}
      {header && !headerRenderer && <Cell data={header} />}
      {rows.map((row, i) => (
        <Cell
          rowHeight={rowHeight}
          key={`${row.name}${i}`}
          data={row[key]}
          cellRenderer={cellRenderer}
        />
      ))}
    </div>
  );
}

Column.defaultProps = {
  column: {},
  rows: []
};

Column.displayName = 'Column';
