import React from 'react';
import { shallow } from 'enzyme';

import DataGrid from './';

import columns from '../../demo/columns.js';
import rows from '../../demo/data.json';

describe('DataGrid', () => {
  it('should render', () => {
    const dom = shallow(<DataGrid />);
    expect(dom).toMatchSnapshot();
  });

  it('should render with data', () => {
    const dom = shallow(<DataGrid columns={columns} rows={rows} />);
    expect(dom).toMatchSnapshot();
  });

  it('should pass down rowHeight to each column', () => {
    const dom = shallow(<DataGrid columns={columns} rowHeight={999} />);
    expect(dom).toMatchSnapshot();
  });
});
