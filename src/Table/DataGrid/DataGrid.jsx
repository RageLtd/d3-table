import React from 'react';
import cn from 'classnames';

import styles from './DataGrid.css';

import Column from '../Column';

export default function DataGrid({ columns, rowHeight, rows }) {
  return (
    <div className={cn(styles.grid)}>
      {columns.map((column, i) => (
        <Column
          key={`${column.key}${i}`}
          rowHeight={rowHeight}
          column={column}
          rows={rows}
        />
      ))}
    </div>
  );
}

DataGrid.defaultProps = {
  rows: [],
  columns: []
};

DataGrid.displayName = 'DataGrid';
