import React from 'react';
import { shallow } from 'enzyme';

import dummyColumns from '../demo/columns';
import dummyRows from '../demo/data.json';

import Table from './Table';

describe('Table', () => {
  it('should render', () => {
    const dom = shallow(<Table />);
    expect(dom).toMatchSnapshot();
  });

  it('should render with data', () => {
    const dom = shallow(<Table columns={dummyColumns} row={dummyRows} />);
    expect(dom).toMatchSnapshot();
  });

  it('should pass down rowHeight', () => {
    const dom = shallow(<Table rowHeight={999} />);
    expect(dom).toMatchSnapshot();
  });
});
