import React from 'react';
import { shallow } from 'enzyme';

import Cell from './Cell';

describe('Cell', () => {
  it('should render', () => {
    const dom = shallow(<Cell />);
    expect(dom).toMatchSnapshot();
  });

  it('should render with data', () => {
    const dom = shallow(<Cell data={"I'm some data"} />);
    expect(dom).toMatchSnapshot();
  });

  it('should render with a custom cellRenderer', () => {
    const dom = shallow(
      <Cell data={"I'm some data"} cellRenderer={data => <div>{data}</div>} />
    );
    expect(dom).toMatchSnapshot();
  });

  it('should add rowHeight to its style object', () => {
    const dom = shallow(<Cell rowHeight={999} />);
    expect(dom).toMatchSnapshot();
  });
});
