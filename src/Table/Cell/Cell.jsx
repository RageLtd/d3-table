import React from 'react';
import cn from 'classnames/bind';

import styles from './Cell.css';

const cx = cn.bind(styles);

export default function Cell({ data, cellRenderer, rowHeight }) {
  return (
    <div style={{ height: rowHeight }} className={cx('cell')}>
      {cellRenderer ? cellRenderer(data) : data}
    </div>
  );
}

Cell.defaultProps = {
  rowHeight: 50
};
