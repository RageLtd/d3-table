import React from 'react';

export default [
  {
    key: 'name',
    header: 'Name',
    headerRenderer: header => <div>I'm a custom {header} header</div>
  },
  {
    key: 'age',
    header: 'Age',
    cellRenderer: data => <div>I'm a custom cell {data}</div>
  },
  { key: 'color', header: 'Color' }
];
