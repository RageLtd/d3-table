import React from 'react';
import { hot } from 'react-hot-loader';

import Table from '../Table';

import dummyData from './data.json';
import dummyColumns from './columns';

function Demo() {
  return (
    <div>
      I'm a demo app
      <Table columns={dummyColumns} data={dummyData} />
    </div>
  );
}

export default hot(module)(Demo);
