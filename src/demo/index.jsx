import React from 'react';
import ReactDOM from 'react-dom';
import Demo from './Demo';

const container = document.createElement('div');
container.id = 'root';

document.querySelector('body').appendChild(container);

const render = Component => ReactDOM.render(<Component />, container);

render(Demo);

if (module.hot) {
  module.hot.accept('./Demo', () => {
    render(Demo);
  });
}
