const path = require('path');

const _ = require('lodash');
const autoprefixer = require('autoprefixer');

const resolve = _.partial(path.resolve, __dirname);

const postcssLoader = {
  loader: 'postcss-loader',
  options: {
    plugins: [
      autoprefixer({
        browsers: 'ie 11, last 2 versions'
      })
    ]
  }
};

const config = {
  entry: { app: ['./src'] },
  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
    alias: {
      components: resolve('../src/components')
    }
  },
  devtool: 'cheap-module-eval-source-map',
  output: {
    path: resolve('../dist'),
    filename: 'bundle.js',
    chunkFilename: '[id].[name].bundle.js'
  },
  plugins: [],
  module: {
    rules: [
      // js and jsx files
      {
        test: /\.jsx?$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true
          }
        },
        exclude: /(node_modules)/
      },
      {
        test: /\.css/,
        include: resolve('../src'),
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[name]__[local]--[hash:base64:5]'
            }
          },
          postcssLoader
        ]
      }
    ]
  },
  devServer: {
    historyApiFallback: true
  }
};

module.exports = config;
