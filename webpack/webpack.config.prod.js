const path = require('path');
const webpack = require('webpack');
const config = require('./webpack.config.base');

const distDir = path.resolve(__dirname, '../dist');

config.entry = {
  library: ['./src/index']
};

config.output = {
  library: 'd3-table',
  path: distDir,
  filename: 'd3-table.js'
};

config.externals = {
  react: 'react',
  'react-dom': 'react-dom'
};

config.plugins.push(
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production')
    }
  })
);

config.mode = 'production';

module.exports = config;
